<?php

/**
 * @entity_class
 * Hooks for Entity Class module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add options to Entity Class widget.
 *
 * This hook lets modules add options to select field in Entity Class widget.
 *
 * @param Drupal\Core\Entity\EntityInterface $entity
 *   Entity edited.
 * @param str $entity_type_id
 *    Entity type ID (like node, user, etc.).
 * @param sstr $bundle
 *    Bundle of entity (like article, page, etc.).
 *
 * @return array
 *   An array of options, like #options key in select element.
 *   @see https://api.drupal.org/api/drupal/core!includes!form.inc/function/form_select_options
 *
 * @see \Drupal\entity_class\Plugin\field\widget\EntityClassWidget::formElement()
 */
function hook_entity_class_options(Drupal\Core\Entity\EntityInterface $entity, $entity_type_id, $bundle) {
  $options = array();

  $options['my-class-1'] = t('My first class');
  $options['my-class-2'] = t('My second class');

  return $options;
}

/**
 * Modify options to Entity Class widget.
 *
 * This hook lets modules modifying options to select field in Entity Class
 * widget added by other modules.
 *
 * @param array $options
 *   An array of options, like #options key in select element.
 *   @see https://api.drupal.org/api/drupal/core!includes!form.inc/function/form_select_options
 * @param array $context
 *   Array with information about current edited entity:
 *   - Drupal\Core\Entity\EntityInterface $context['entity']
 *       Entity edited.
 *   - str $context['entity_type_id']
 *       Entity type ID (like node, user, etc.).
 *   - str $context['bundle']
 *       Bundle of entity (like article, page, etc.).
 *
 * @see \Drupal\entity_class\Plugin\field\widget\EntityClassWidget::formElement()
 */
function hook_entity_class_options_alter(array &$options, array $context) {
  $options['my-class-2'] = t('My 2nd class');
}
