<?php

namespace Drupal\entity_class;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the field_ui module.
 */
class EntityClassPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new FieldUiPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity.manager'));
  }

  /**
   * Returns an array of Entity Class permissions.
   *
   * @return array
   */
  public function permissions() {
    $permissions = array();

    foreach (_entity_class_managed_types() as $entity_type_id) {
      $entity_type = $this->entityManager->getDefinition($entity_type_id);
      $bundles = $this->entityManager->getBundleInfo($entity_type_id);
      foreach($bundles as $bundle_id => $bundle) {
        $placeholders = array(
          '@entity_type_label' => $entity_type->getLabel(),
          '@bundle_label' => $bundle['label'],
        );
        $permissions[$entity_type_id . ' edit entity classes for ' . $bundle_id] = array(
          'title' => $this->t('@entity_type_label: edit entity classes for "@bundle_label"', $placeholders),
        );
        $permissions[$entity_type_id . ' edit custom entity classes for ' . $bundle_id] = array(
          'title' => $this->t('@entity_type_label: edit custom entity classes for "@bundle_label"', $placeholders),
        );
      }
    }

    return $permissions;
  }

}
