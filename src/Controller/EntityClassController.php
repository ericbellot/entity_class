<?php

/**
 * @file
 * Contains \Drupal\field_example\Controller\FieldExampleController.
 */

namespace Drupal\entity_class\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for dblog routes.
 */
class EntityClassController extends ControllerBase {

  /**
   * A simple page to explain to the developer what to do.
   */
  public function description() {
    return array(
      '#markup' => t(
        "Entity Class provides a field to add HTML classes at every entity instance."),
    );
  }

}
