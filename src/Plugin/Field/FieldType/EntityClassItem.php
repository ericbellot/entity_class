<?php

/**
 * @file
 * Contains Drupal\entity_class\Plugin\Field\FieldType\EntityClass.
 *
 * Define new field type Entity Class.
 */

namespace Drupal\entity_class\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'entity_class' field type.
 *
 * @FieldType(
 *   id = "entity_class",
 *   label = @Translation("Entity class"),
 *   module = "entity_class",
 *   description = @Translation("Add classes to entity."),
 *   no_ui = TRUE,
 *   default_widget = "entity_class",
 * )
 */
class EntityClassItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Entity Class'));

    return $properties;
  }


  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => 255,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }
}
