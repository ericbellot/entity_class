<?php

/**
 * @file
 * Contains \Drupal\entity_class\Plugin\field\widget\EntityClassWidget.
 */

namespace Drupal\entity_class\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_class' widget.
 *
 * @FieldWidget(
 *   id = "entity_class",
 *   module = "entity_class",
 *   label = @Translation("Entity class"),
 *   field_types = {
 *     "entity_class"
 *   }
 * )
 */
class EntityClassWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += array(
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#element_validate' => array(
        array($this, 'validate'),
      ),
    );



    $entity = $items->getEntity();
    $hook_data = array(
      'entity' => $entity,
      'entity_type_id' => $entity->getEntityTypeId(),
      'bundle' => $entity->bundle(),
    );

    $classes = isset($items[$delta]->value) ? explode(' ', $items[$delta]->value) : array();

    $entity_classes = \Drupal::moduleHandler()
      ->invokeAll('entity_class_options', $hook_data);
    \Drupal::moduleHandler()
      ->alter('entity_class_options', $entity_classes, $hook_data);

    if ($entity_classes) {
      $options = array();

      foreach($entity_classes as $entity_class => $entity_class_values) {
        $options[$entity_class] = $entity_class_values['title'];
      }

      $selected_values = array();
      // We load default values, if new entity.
      $entity_id = $entity->id();
      if(!$entity_id) {
        foreach($entity_classes as $entity_class => $entity_class_values) {
          if($entity_class_values['default_value']) {
            $selected_values[] = $entity_class;
          }
        }
      }
      // If entity has ID, so if exists, we load saved values.
      else{
        foreach ($classes as $index => $class) {
          if (array_key_exists($class, $entity_classes)) {
            $selected_values[] = $class;
            unset($classes[$class]);
          }
        }
      }

      $element['entity_class_select'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Select options to enable'),
        '#default_value' => $selected_values,
        '#options' => $options,
        '#required' => FALSE,
        '#multiple' => TRUE,
        '#weight' => 0,
      );
    }

    $user = \Drupal::currentUser();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle_id = $entity->bundle();
    if ($user->hasPermission($entity_type_id . ' edit custom entity classes for ' . $bundle_id, $user)) {
      $element['entity_class_custom'] = array(
        '#type' => 'textfield',
        '#title' => t('Add custom options'),
        '#description' => t('Enter options (HTML classes) to modify the display of entity.'),
        '#default_value' => implode(' ', $classes),
        '#required' => FALSE,
        '#weight' => 10,
      );
    }
    return $element;
  }

  /**
   * Validate the class item widget.
   */
  public function validate(&$element, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    $classes = array();
    if(isset($element['entity_class_custom']['#value'])) {
      $custom_value = $element['entity_class_custom']['#value'];
//    $custom_value = $form_state->getValue(array('entity_class', 0 ,'entity_class_custom'));
      if (!empty($custom_value)) {
        $custom_classes = preg_split('#\s+#', $custom_value);
        foreach ($custom_classes as $class) {
          if (!preg_match('#^-?[_a-zA-Z]+[_a-zA-Z0-9-]*$#', $class)) {
            $placeholders = array(
              '!class' => $class,
            );
            $form_state->setError($element['entity_class_custom'], t("The class <code>!class</code> must respect HTML syntax for classes.", $placeholders));
          }
          else {
            $classes[] = $class;
          }
        }
      }
      if ($form_state->hasAnyErrors()) {
        return;
      }
    }
    if(isset($element['entity_class_select']['#value'])) {
      $selected_value = $element['entity_class_select']['#value'];
//    $selected_value = $form_state->getValue(array('entity_class', 0 ,'entity_class_select'));
      if (!empty($selected_value)) {
        foreach ($selected_value as $class => $enabled) {
          if ($enabled) {
            $classes[] = $class;
          }
        }
      }
    }
    $value = implode(' ', array_unique($classes));


    $parents = $element['#parents'];

    // The Entity Class widget use fields different of simple Entity Class item
    // which has only a string. To store data we modify structure of
    // element form to simulate the item structure. And we set the
    // value to form state.
    if ($value) {
      $element['#parents'] = array_merge($parents, array('value'));
      $element['#value'] = $value;
      $form_state->setValueForElement($element, $value);
    }

    // We remove widget fields, yet useless.
    $custom_field_parents = array_merge($parents, array('entity_class_custom'));
    $form_state->unsetValue($custom_field_parents);
    $select_field_parents = array_merge($parents, array('entity_class_select'));
    $form_state->unsetValue($select_field_parents);
  }
}
