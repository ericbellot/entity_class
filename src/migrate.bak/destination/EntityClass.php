<?php

namespace Drupal\path\Plugin\migrate\destination;

use Drupal\entity_class\EntityClassStorage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * @MigrateDestination(
 *   id = "entity_class"
 * )
 */
class EntityClass extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The entity_class storage service.
   *
   * @var \Drupal\entity_class\EntityClassStorage $entityClassStorage
   */
  protected $entityClassStorage;

  /**
   * Constructs an entity destination plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\entity_class\EntityClassStorage $alias_storage
   *   The alias storage service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, AliasStorage $entity_class_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->entityClassStorage = $entity_class_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_class.entity_class_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = array()) {

    $path = $this->entityClassStorage->save(
      $row->getDestinationProperty('source'),
      $row->getDestinationProperty('alias'),
      $row->getDestinationProperty('langcode'),
      $old_destination_id_values ? $old_destination_id_values[0] : NULL
    );

    return array($path['pid']);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['pid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'pid' => 'The path id',
      'source' => 'The source path.',
      'alias' => 'The URL alias.',
      'langcode' => 'The language code for the URL.',
    ];
  }

}
