<?php
/**
 * @file
 * Contains \Drupal\entity_class\From\ClearAllForm.
 *
 * Form used to delete Entity Class data for all entities to allow module
 * uninstall.
 */

namespace Drupal\entity_class\Form;

use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the Flag clear all form.
 */
class ClearAllForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_class_clearall_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Clear all entity class');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.modules_uninstall');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will delete all classes in all entities in preparation to uninstall Entity Class module. This operation cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Clear all');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach (_entity_class_managed_types() as $entity_type_id) {
      $entity_type = \Drupal::entityTypeManager()
        ->getDefinition($entity_type_id);
      if ($entity_type) {
        $placeholders = array(
          '@entity_type_label' => $entity_type->getLabel(),
        );
        $batch = array();
        $batch['title'] = $this->t('Clearing Entity Class data for @entity_type_label', $placeholders);
        $batch['operations'][] = array(
          array(__CLASS__, 'clearEntityClass'),
          array($entity_type_id)
        );
        $batch['progress_message'] = $this->t('Clearing Entity Class data for @entity_type_label', $placeholders);
        batch_set($batch);
      }
    }

    drupal_set_message($this->t(
      'Entity Class data has been cleared. <a href="@uninstall-url">Proceed with uninstallation.</a>',
      [
        '@uninstall-url' => Url::fromRoute('system.modules_uninstall')
          ->toString(),
      ]
    ));
  }

  /**
   * Batch method to reset all entity class.
   */
  public static function clearEntityClass($entity_type_id, &$context) {
    // First, set the number of flags we'll process each invocation.
    $batch_size = 100;

    // If this is the first invocation, set our index and maximum.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = \Drupal::entityQuery($entity_type_id)
        ->count()
        ->execute();
    }

    // Get the next batch of flags to process.
    $query = \Drupal::entityQuery($entity_type_id);
    $query->range($context['sandbox']['progress'], $batch_size);
    $ids = $query->execute();


    // Remove Entity Class data in entities (= set to NULL value).
    $entities = \Drupal::entityTypeManager()
      ->getStorage($entity_type_id)
      ->loadMultiple($ids);
    foreach ($entities as $id => $entity) {
      $entity->set('entity_class', NULL);
      $entity->save();
    }

    // Increment our progress.
    $context['sandbox']['progress'] += $batch_size;

    // If we still have entity class to process, set our progress percentage.
    if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }
}
